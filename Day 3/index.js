const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
require("dotenv").config();

const port = process.env.NODE_PORT;

const app = express();

function renameFile(oldName) {
  const [filename, extension] = oldName.split(".");
  return `${filename}-${Date.now()}.${extension}`;
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, "uploads/"),
  filename: (req, file, cb) => cb(null, renameFile(file.originalname)),
});

const uploads = multer({ storage: storage });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.post("/register", uploads.single("profilePicture"), (req, res) => {
  res.send(req.body);
});

app.listen(port, () => console.log(`this app running on port ${port}`));
